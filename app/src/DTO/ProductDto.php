<?php

declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class ProductDto implements DtoInterface
{
    public function __construct(
        #[Assert\Type(type: 'string')]
        #[Assert\Length(max: 255)]
        #[Assert\NotBlank]
        #[Assert\NotNull]
        public $name
    ) {
    }
}
