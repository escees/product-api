<?php

namespace App\Notification;

use App\Entity\Product;

interface NotificationInterface
{
    public function send(Product $product): void;
}
