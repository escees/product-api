<?php

declare(strict_types=1);

namespace App\Notification;

use App\Entity\Product;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface as SymfonyMailer;
use Symfony\Component\Mime\Email;

class Mailer implements NotificationInterface
{
    public function __construct(
        private SymfonyMailer $mailer,
        private string $receiver
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function send(Product $product): void
    {
        $this->mailer->send(
            (new Email())
                ->from('send@example.com')
                ->to($this->receiver)
                ->subject('Product created!')
                ->html(sprintf('<p>Product %s was created</p>', $product->getName()))
        );
    }
}
