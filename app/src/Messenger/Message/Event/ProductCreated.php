<?php

declare(strict_types=1);

namespace App\Messenger\Message\Event;

class ProductCreated
{
    public function __construct(
        public readonly int $productId
    ) {
    }
}
