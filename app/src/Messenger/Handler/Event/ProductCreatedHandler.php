<?php

declare(strict_types=1);

namespace App\Messenger\Handler\Event;

use App\Messenger\Message\Event\ProductCreated;
use App\Notification\NotificationInterface;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ProductCreatedHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepository $repository,
        private NotificationInterface $mailer
    ) {
    }

    public function __invoke(ProductCreated $productCreated)
    {
        $this->mailer->send($this->repository->find($productCreated->productId));
    }
}
