<?php

declare(strict_types=1);

namespace App\Messenger\Handler\Query;

use App\Entity\Product;
use App\Messenger\Message\Query\GetProductList;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetProductListHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepository $repository,
    ) {
    }

    /**
     * @return Product[]
     */
    public function __invoke(GetProductList $getProductList): array
    {
        return $this->repository->findAllOrderedByCreatedAtDescending();
    }
}
