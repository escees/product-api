<?php

declare(strict_types=1);

namespace App\Controller;

use App\DTO\ProductDto;
use App\Entity\Product;
use App\Factory\ProductFactory;
use App\Messenger\Message\Event\ProductCreated;
use App\Messenger\Message\Query\GetProductList;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    #[Route(path: '/api/product/list', name: 'api_product_list', methods: ['GET'])]
    public function list(MessageBusInterface $queryBus): JsonResponse
    {
        $envelope = $queryBus->dispatch(new GetProductList());
        /** @var HandledStamp $handledStamp */
        $handledStamp = $envelope->last(HandledStamp::class);

        return $this->json(
            $handledStamp->getResult()
        );
    }

    #[Route(path: 'api/product/create', name: 'api_product_create', methods: ['POST'])]
    public function create(
        ProductDto $productDto,
        MessageBusInterface $bus
    ): JsonResponse {
        $product = ProductFactory::create($productDto->name);
        $this->entityManager->persist($product);
        $this->entityManager->flush();

        $bus->dispatch(new ProductCreated($product->getId()));

        return $this->json(
            $product,
            Response::HTTP_CREATED
        );
    }

    #[Route(path: 'api/product/{product}/update', name: 'api_product_update', methods: ['PUT'])]
    public function update(Product $product, ProductDto $productDto): JsonResponse
    {
        $product->setName($productDto->name);
        $this->entityManager->flush();

        return $this->json(
            $product,
            Response::HTTP_NO_CONTENT
        );
    }

    #[Route(path: 'api/product/{product}/delete', name: 'api_product_delete', methods: ['DELETE'])]
    public function delete(Product $product): JsonResponse
    {
        $this->entityManager->remove($product);
        $this->entityManager->flush();

        return $this->json([]);
    }
}
