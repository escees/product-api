<?php

declare(strict_types=1);

namespace App\ArgumentValueResolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Exception\MissingConstructorArgumentsException;
use Symfony\Component\Serializer\Exception\NotNormalizableValueException;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DtoValueResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private readonly DenormalizerInterface $denormalizer,
        private readonly ValidatorInterface $validator
    ) {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() && str_contains(mb_strtolower($argument->getType()), 'dto');
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        try {
            $dto = $this->denormalizer->denormalize($request->toArray(), $argument->getType());
        } catch (NotNormalizableValueException) {
        } catch (MissingConstructorArgumentsException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        $errors = $this->validator->validate($dto);
        if (count($errors)) {
            throw new BadRequestHttpException((string) $errors);
        }

        yield $dto;
    }
}
