<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Product;

class ProductFactory
{
    public static function create(string $name): Product
    {
        return new Product($name);
    }
}
