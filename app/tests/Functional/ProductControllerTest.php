<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\DataFixtures\ProductFixtures;
use App\Repository\ProductRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerTest extends WebTestCase
{
    private AbstractDatabaseTool $databaseTool;
    private KernelBrowser $client;
    private ProductRepository $repository;

    public function setUp(): void
    {
        $this->client = static::createClient();

        $container = static::getContainer();

        $this->databaseTool = $container->get(DatabaseToolCollection::class)->get();
        $this->repository = $container->get(ProductRepository::class);

        $this->databaseTool->loadFixtures([]); // refreshing database could be achieved in many ways, this is one of them
        $this->databaseTool->loadFixtures([ProductFixtures::class]);
    }

    public function testList(): void
    {
        $this->client->request('GET', '/api/product/list');

        self::assertResponseIsSuccessful();

        $response = $this->client->getResponse();
        $decodedResponse = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        self::assertCount(20, $decodedResponse);
    }

    public function testCreate(): void
    {
        $this->client->request(
            'POST',
            '/api/product/create',
            [],
            [],
            [],
            json_encode([
                'name' => 'New product' . bin2hex(random_bytes(5))
            ])

        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(Response::HTTP_CREATED);
        self::assertCount(21, $this->repository->findAll());
    }

    public function testUpdate(): void
    {
        $productName = 'New product' . bin2hex(random_bytes(5));

        $this->client->request(
            'PUT',
            '/api/product/19/update',
            [],
            [],
            [],
            json_encode([
                'name' => $productName
            ])
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(Response::HTTP_NO_CONTENT);
        self::assertSame($productName, $this->repository->find(19)->getName());
    }

    public function testDelete(): void
    {
        $this->client->request(
            'DELETE',
            '/api/product/20/delete',
        );

        self::assertResponseIsSuccessful();
        self::assertResponseStatusCodeSame(Response::HTTP_OK);
        self::assertCount(19, $this->repository->findAll());
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCreateFailed(mixed $name, string $violation)
    {
        $this->client->request(
            'POST',
            '/api/product/create',
            [],
            [],
            [],
            json_encode([
                'name' => $name
            ])

        );

        self::assertResponseStatusCodeSame(Response::HTTP_BAD_REQUEST);
        self::assertStringContainsString($violation, $this->client->getResponse()->getContent());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->databaseTool);
    }

    public function dataProvider(): \Generator
    {
        yield [123,'This value should be of type string'];
        yield ['', 'This value should not be blank'];
        yield [null, 'This value should not be null'];
    }
}
