# product-api

Simple Product API written using CQRS. When product is created through API there is an email message sent.
Technologies:
- CQRS
- SF 6 and PHP8.1
- RabbitMQ for transport
- and more..

## Getting started

```
cp .env.dist .env
cp app/.env app/.env.local // adjust db credentials accordingly to docker .env
docker-compose up --build -d  // build containers 
docker-compose exec php-fpm bash // enter php container
composer install //install dependencies
bin/console d:m:m // run db migrations
bin/console d:d:c -e test // create test database
bin/console d:s:c -e test // create test schema
bin/console d:fix:load  // load dev fixtures
bin/console d:fix:load -e test // load test fixtures
vendor/bin/phpunit // run tests
```
1. There is no auth for the api, but usually it should be JWT or JWE token(Bearer) as Authorization header.
2. With current config, if you set `MAILER_DSN` env you will be able to asynchronously send email about product created using rabbitmq queue.
3. To access RabbitMQ dashboard go to localhost:8053 and login with guest:guest credentials
4. DB can be accessed at localhost:3351
5. Before you create product through API, you should set up messenger worker with command bin/console mess:consume async (if you don't want to send email you can just dump productId to see if transport works)

## Available product-api routes


 - List products
```
GET http://localhost:9999/api/product/list
Content-Type: application/json
Accept: application/json
```
- Create product
```
POST http://localhost:9999/api/product/create
Content-Type: application/json
Accept: application/json

{
    "name": "Product name"
}
```
- Update product
```
PUT http://localhost:9999/api/product/{id}/update
Content-Type: application/json
Accept: application/json

{
    "name": "Product name"
}
```
- Delete product
```
PUT http://localhost:9999/api/product/{id}/delete
Content-Type: application/json
Accept: application/json
```
